#include <iostream>

using namespace std;


int main (int argn, char *args []) {

	int nombre = 24;
	int *pointeur = &nombre;

	//Affiche nombre = 24
	cout << "nombre = " << nombre << endl;

	//Affiche pointeur = addresse a laquelle se trouve la variable nombre
	cout << "pointeur = " << pointeur << endl;

	//Affiche *pointeur = 24 (valeur de nombre)
	cout << "*pointeur = " << *pointeur << endl;

	*pointeur = 15;
	//on a changer la valeur de nombre
	//nombre = 15 maintenant
	//va afficher nombre = 15
	cout << "nombre = " << nombre << endl;

	return 0;
}
