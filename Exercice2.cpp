#include <iostream>
#include <string>

using namespace std;

//Fonction sans reference
void echangerNom1 (string nom1, string nom2) {

	string temp = nom1;
	nom1 = nom2;
	nom2 = temp;

}


//Fonction avec reference
void echangerNom2 (string& nom1, string& nom2) {

	string temp = nom1;
	nom1 = nom2;
	nom2 = temp;

}


int main () {

	string nom1 = "Merveille";
	string nom2 = "Elodie";

	//appelle de la premiere fonction
	//devrais afficher le nom1 : Merveille
	//		   le nom2 : Elodie
	echangerNom1(nom1, nom2);
	cout << "FONCTION 1 SANS REFERENCE" << endl ;
	cout << "le nom1 : " << nom1 << endl << "le nom2 : " << nom2 << endl << endl;

	//appelle de la deuxieme fonction
        //devrais afficher le nom1 : Elodie
	//		   le nom2 : Merveille
        echangerNom2(nom1, nom2);
	cout << "FONCTION 2 AVEC REFERENCE" << endl ;
        cout << "le nom1 : " << nom1 << endl << "le nom2 : " << nom2 << endl;

	return 0;

}
