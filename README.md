Ce projet me permet de m'entrainer a faire des exercices afin de comprendre 
la programmation en C++

# Exercice1

	Operateurs
> . Il s'agit juste de demander des valeurs (2) a un utilisateur et de faire differentes operations. Affiche le resultat final

# Exercice2

	References
> . En conclusion, si on veut utiliser une fonction ailleur, on devrait 
>   declarer ses arguments par reference, sinon les arguments sont juste copies
>   et ne servent a rien dans une autre fonction.


# Exercice3

	Valeurs par defaut pour les arguments
> . Toujours mettre les valeurs par defaut a partir de la droite (fin des arguments)

> . Mieux ou plutot doit etre mis au niveau des prototypes.
	
	> . Exemple : prototype : int nombreSeconde(int heure, int minute, int seconde = 60);
	
	> .           definition : int nombreSeconde(int heure, int minute, int seconde) {//}

> . Tant qu'on indique les arguments obligatoires (sans valeurs par defaut), on n'est pas oblige d'indiquer les autres arguments


# Exercice4

	POINTEURS
> . Pour declarer un pointeur : type *nomDuPointeur = nullptr;

> . Pour mettre l'addresse d'une variable dans un pointeur on utilise &
	(nomDuPointeur = &variable), contient l'adresse de la variable

> . Pour modifier la valeur de la variable en utilisant le pointeur
	(*nomDuPointeur = nouvelleValeur)

> . Un pointeur est une variable qui pointe sur une autre variable

> . on doit toujour l'initialiser par nullptr;


# Exercice5

	LES FICHIERS 
> . Ecrire dans un fichier :

	> . Ajouter la bibliotheque <fstream> (#include <fstream>)

	> . ofstream nomDuChemin ("cheminDuFichierAEcrire ou nomDuFichier si dans le meme dossier"); : pour declarer un flux vers le fichier a ecrire

	> . Autre maniere de declarer une variable de type ofstream (pour ecrire toujours)

		> . string const nomDuChemin = "cheminDuFichierAEcrire ou nomDuFichier si dans le meme dossier"
	
		>   ofstream leFlux (nomDuChemin.c_str());

	> . Pour ajouter a la suite dans un fichier au lieu de supprimer ce qu'il y'avait dans le fichier et reecrire le nouveau contenu, on fait ofstream leFlux(nomFichier.c_str(), ios::app);

> . Lire dans un fichier :

	> . On declare le flux avec ifstream

	> . Il y'a 3 manieres de lire un fichier

		> . 1. getline(); lis ligne par ligne

		> . 2. lire mot par mot (en utilisant >>)

		> . 3. get(); lire caractere par caractere
