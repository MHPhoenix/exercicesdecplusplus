#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main (int argn, char *args []) {

	//ECRIRE DANS UN FICHIER

	//chemin relatif
	string const nomFichier = "monFichier2.txt";

	//Pas besoin de faire appelle a la fonction c_str()
	//chemin relatif
	ofstream cheminAccesFichier ( "monFichier.txt" );

	//Oblige de faire appelle a la fonction c_str()
	//parce qu'on utilise une string comme nom du chemin
	ofstream cheminAccesFichier2 ( nomFichier.c_str() );

	//Ecrire dans le premier fichier
	if (cheminAccesFichier) {

		cout << "YEEEEEAH tout s'est bien passe pour l'ecriture du fichier 1 " << endl << endl;

		cheminAccesFichier << "	Bonjour, " << endl << endl;
		cheminAccesFichier << "	Ceci est mon premier fichier ecrit en c++" << endl;
		cheminAccesFichier << "Je ne suis pas encore morte de traumatisme depuis le debut de ce programme (BAC)" << endl << endl;
		cheminAccesFichier << "Youpiiiiii !!!" << endl;

	} else {
		cout << "ERREUR : tu etais bien pourtant parti pour l'ouverture de ton fichier" << endl << endl;
	}

	//Ecrire dans le deuxieme fichier
        if (cheminAccesFichier2) {

		cout << "OUIIIIIIIIIIIIIII tout s'est bien passe pour l'ecriture du fichier 2 " << endl << endl;

                cheminAccesFichier2 << " Bonjour, " << endl << endl;
                cheminAccesFichier2 << " Ceci est mon deuxieme fichier ecrit en c++" << endl;
                cheminAccesFichier2 << "Je ne suis pas encore morte de traumatisme depuis le debut de ce programme (BAC)" << endl << endl;
                cheminAccesFichier2 << "Youpiiiiii !!!" << endl;

        } else {
                cout << "ERREUR : tu etais bien pourtant parti pour l'ouverture de ton fichier 2" << endl;
        }


	//Lire DANS UN FICHIER


        //chemin relatif
        ifstream fichier ( "monFichier.txt" );

        if (fichier) {

		//avec getline
		cout << "LECTURE AVEC GETLINE()" << endl;
		string ligne = " ";
		while ( getline (fichier, ligne) ) {
			cout << ligne << endl;
		}

		cout << " " << endl;
		//lecture en utilisant mot par mot
		cout << "LECTURE AVEC (>>) LIGNE PAR LIGNE" << endl;
		string mot = " ";
		while (fichier >> mot ) {
			cout << mot << endl;
		}

		cout << " " << endl;
                //lecture caractere par caractere
                cout << "LECTURE AVEC get() CARACTERE PAR CARACTERE" << endl;
                char caractere = ' ';
                while ( fichier.get(caractere) ) {
                        cout << caractere << endl;
                }

        } else {
                cout << "ERREUR : tu etais bien pourtant parti pour la lecture de ton fichier" << endl;
        }

	return 0;
}
