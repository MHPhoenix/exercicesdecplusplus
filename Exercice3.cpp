#include <iostream>

using namespace std;

//prototypes
int nombreDeJours1 (int heures);
int nombreDeJours2 (int heures = 48);

int main () {

	//sans valeurs par defaut
	//retourne 2 jours
	cout << "nombre de jours ecoule : " << nombreDeJours1(48) << endl ;

	//avec valeurs par defaut
        //retourne 2 jours
        cout << "nombre de jours ecoule : " << nombreDeJours2() << endl ;

	return 0;

}


//Definitions des fonctions

//fonction sans valeur par defaut pour argument
int nombreDeJours1 (int heures) {

	int jours = 0;
	int reste = heures;

	if ( heures % 24 == 0 ) {

		while ( reste - 24 != 0 ) {

			reste = reste - 24;

			jours += 1;

		}

		jours += 1;

	}

	return jours;

}


//fonction avec valeur par defaut pour argument
int nombreDeJours2 (int heures) {

        int jours = 0;
        int reste = heures;

        if ( heures % 24 == 0 ) {

                while ( reste - 24 != 0 ) {

                        reste = reste - 24;

                        jours += 1;

		}

		jours += 1;

	}

	return jours;

}

